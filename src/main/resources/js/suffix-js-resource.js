AJS.$(document).ready(function(){
    //var VolumeID        = "10007";
    //var ProbabilityBasedVolumeID  = "10018";
    //var ProbabilityID       = "10017";

    var priceID        = "customfield_10000";
    //TODO Get the list of fields defined by the admin from the plugin setting page.

  	JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason){
	    loadActionView(reason);
	});

	function loadActionView(reason){
	    var EuroSymbol       = '€';
	    //Price
	    /* LIST VIEW */
        var priceHeader    = AJS.$("th[data-id='"+priceID+"']");
        var priceRows      = AJS.$("tbody tr td."+priceID);
	    /* DETAIL VIEW */
	    var priceDetails   = AJS.$('#'+priceID+'-val');
        FormatMe(priceHeader, priceRows, priceDetails, reason, EuroSymbol);
     }

    function FormatMe (ElementHeader, ElementRows, ElementDetail, reason, symbol){
        if (IsListView(reason)){
            // TODO Should we align right or left or center , get this info from the plugin settings
            //ElementRightAlign(ElementHeader);
            ElementRows.each(function(){
                //ElementRightAlign($(this));
                ElementAppendSymbol($(this), symbol);
            });
        }

        if (IsDetailedView(reason)){
            // if element  exist then format the detailed view
            if (ElementDetail.length > 0)
                FormatCustomFieldDetailedView(ElementDetail, symbol);
        }
    }


     function IsListView(reason){
       if (reason == "issueTableRefreshed" || reason == "layoutSwitcherReady" || reason == "issueTableRowRefreshed" || reason == "dialogReady")
		    return true;
        else
		    return false;
     }

     function IsDetailedView(reason) {
        if (reason == "layoutSwitcherReady" || reason == "pageLoad" || reason == "panelRefreshed" || reason == "dialogReady")
     		 return true;
        else
     		 return false;
     }

     function FormatCustomFieldDetailedView(CustomField, symbol)
     	{
     	      ElementAppendSymbol(CustomField, symbol);
               //CustomField.css('font-weight','bold');
     	}

     function ElementRightAlign(Element){
        Element.css('text-align','right');
     }

     function ElementAppendSymbol(Element, symbol){
         console.log("1 #######################################################");
         if(Element.text().indexOf(symbol) >= 0 || Element.text().length == 0)
            {
            return;
            }
         else {
            Element.append(symbol);
            Element.css("white-space", "nowrap");
            }
     }
	loadActionView("issueTableRefreshed");
});