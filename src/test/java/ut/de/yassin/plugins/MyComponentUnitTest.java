package ut.de.yassin.plugins;

import org.junit.Test;
import de.yassin.plugins.api.MyPluginComponent;
import de.yassin.plugins.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}